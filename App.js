import React from 'react';
import { SafeAreaView, StatusBar } from 'react-native';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';

import rootReducer from './src/reducers';
import RootNav from './src/RootNav';

const App: () => React$Node = () => {
  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView style={{ flex: 1 }}>
        <Provider
          store={createStore(rootReducer, compose(applyMiddleware(thunk)))}>
          <RootNav />
        </Provider>
      </SafeAreaView>
    </>
  );
};

export default App;
