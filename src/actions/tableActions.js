import { uniq } from 'lodash';
import { API } from '../services';
import { ActionTypes } from '../constants';
import { tableUtils } from '../utils';
import { homePlanetParser, pilotParser, vehicleParser } from '../parsers';

const getVehicles = async () => {
  const vehicles = await API.fetchAllVehicles();

  // filter only those with pilots
  const vehiclesWithPilots = vehicles.filter(
    (vehicle) => vehicle.pilots.length,
  );

  return vehiclesWithPilots.map(vehicleParser.parseVehicle);
};

const getPilotsByVehicles = async (vehicles) => {
  const pilots = tableUtils.getArraysByAndConcat(vehicles, 'pilots');
  const uniquePilotsUrls = uniq(pilots);

  const pilotsDetails = await API.fetchByUrls(uniquePilotsUrls);

  return pilotsDetails.map(pilotParser.parsePilot);
};

const getHomePlanetsByPilots = async (pilots) => {
  const homePlanets = pilots.map((pilot) => pilot.homeworld);
  const uniqueHomePlanetsUrls = uniq(homePlanets);

  const homePlanetsDetails = await API.fetchByUrls(uniqueHomePlanetsUrls);

  return homePlanetsDetails.map(homePlanetParser.parseHomePlanet);
};

function getVehiclesWithPilots() {
  return async (dispatch) => {
    try {
      dispatch({ type: ActionTypes.GET_VEHICLES_WITH_PILOTS_START });

      // fetch the vehicles
      const vehicles = await getVehicles();

      // fetch pilots details by vehicles
      const pilots = await getPilotsByVehicles(vehicles);

      // fetch relevant homePlanets
      const homePlanets = await getHomePlanetsByPilots(pilots);

      // combine data
      const vehiclesWithPlanets = vehicles;

      /**
       * define first vehicle as vehicle with max population, and replace if one with greater population is found
       **/
      let vehicleWithMaxPopulation = vehiclesWithPlanets[0];

      // There is no need to go through the pilots again

      // We will go through vehicles and for each vehicle we will find the homeplanets which have its pilots as residents
      vehiclesWithPlanets.forEach((vehicle, index) => {
        /**
         * for each vehicle we'll go through its pilots
         * and we'll add each pilot's home planet to
         * the vehicle homePlanets object
         */
        vehicle.pilots.forEach((pilotUrl) => {
          const homePlanet = tableUtils.findObjectBy(
            pilotUrl,
            homePlanets,
            'residents',
          );

          // here we add the home planet to the vehicle object
          vehicle.homePlanets[homePlanet.name] = homePlanet;
        });

        vehicle.totalPopulation = tableUtils.sumObjectsByField(
          vehicle.homePlanets,
          'population',
        );

        // filter relevant pilots and get their names
        vehicle.pilotsNames = tableUtils.filterByAndGetField(
          vehicle.pilots,
          pilots,
          'url',
          'name',
        );

        vehicleWithMaxPopulation = tableUtils.getGreaterBy(
          vehicle,
          vehicleWithMaxPopulation,
          'totalPopulation',
        );
      });

      dispatch({
        type: ActionTypes.GET_VEHICLES_WITH_PILOTS_SUCCESS,
        payload: {
          vehicleWithMaxPopulation,
        },
      });
    } catch (error) {
      console.log('error', error);
      dispatch({ type: ActionTypes.GET_VEHICLES_WITH_PILOTS_ERROR });
    }
  };
}

export default {
  getVehiclesWithPilots,
};
