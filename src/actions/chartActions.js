import { API } from '../services';
import { ActionTypes } from '../constants';
import { chartUtils } from '../utils';
import { homePlanetParser } from '../parsers';

const getPlanetsByNames = (names) => async (dispatch) => {
  try {
    dispatch({ type: ActionTypes.GET_PLANETS_FOR_CHARTS_START });

    const dataObjectsArray = await API.fetchPlanetsByNames(names);
    const planets = dataObjectsArray.map((data) => {
      const planet = data.results[0];
      return homePlanetParser.parseHomePlanet(planet);
    });

    const maxPopulation = chartUtils.getMaxBy(planets, 'population');

    dispatch({
      type: ActionTypes.GET_PLANETS_FOR_CHARTS_SUCCESS,
      payload: { planets, maxPopulation },
    });

    console.log('planets', planets);
  } catch (error) {
    console.log('error', error);
    dispatch({ type: ActionTypes.GET_PLANETS_FOR_CHARTS_ERROR });
  }
};

export default {
  getPlanetsByNames,
};
