/**
 * 
 * @param {Array} flatArray - array to search in
 * @param {Array} deepArray - array of objects to get the field to search
 * @param {String} field - field to search
 * @param {String} fieldToReturn - field to return
 */
const filterByAndGetField = (flatArray, deepArray, field, fieldToReturn) => {
  return deepArray
    .filter((obj) => flatArray.includes(obj[field]))
    .map((obj) => obj[fieldToReturn]);
};

/**
 * Sum sub-objects by field
 * 
 * @param {Object of objects} object - Object of objects
 * @param {String} field - field to sum
 */
const sumObjectsByField = (object, field) => {
  return Object.values(object)
    .map((subObj) => subObj[field])
    .reduce((a, b) => a + b, 0);
};

/**
 * Get the greatest object by field
 * 
 * @param {Object} objA - first object to compare
 * @param {Object} objB - second object to compare
 * @param {String} field - which field to compare
 */
const getGreaterBy = (objA, objB, field) => {
  if (objA[field] > objB[field]) {
    return objA;
  }
  return objB;
};

/**
 * Look for element in array by field
 * 
 * @param {*} element - element to find
 * @param {Array} array - array to find in
 * @param {String} field - which field in the array to compare
 */
const findObjectBy = (element, array, field) => {
  return array.find((obj) => obj[field].includes(element));
};

/**
 * Get all the arrays reside in the given field in
 * the given array and concat them
 * 
 * @param {Array} array - array to look for the field
 * @param {String} field - field to get the arrays by
 */
const getArraysByAndConcat = (array, field) => {
	return [].concat(...array.map((vehicle) => vehicle[field]));
}

export default {
	filterByAndGetField,
	sumObjectsByField,
	getGreaterBy,
	findObjectBy,
	getArraysByAndConcat,
};