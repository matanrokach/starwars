/**
 * Get object with the max value of given field
 *
 * @param {Array} array - array of objects to compare
 * @param {String} field - field to compare by
 */
const getMaxBy = (array, field) => {
  return Math.max(...array.map((obj) => obj[field]));
};

export default {
  getMaxBy,
};
