import { toNumber } from 'lodash';

const parseHomePlanet = (rawHomePlanet) => {
  const { name, residents, population } = rawHomePlanet;

  return {
    name,
    residents,
    population: toNumber(population) || 0,
  };
};

export default { parseHomePlanet };
