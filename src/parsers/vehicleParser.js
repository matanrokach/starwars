const parseVehicle = (rawVehicle) => {
  const { name, pilots } = rawVehicle;

  return {
    name,
    pilots,
    homePlanets: {},
  };
};

export default { parseVehicle };
