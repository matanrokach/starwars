const parsePilot = (rawPilot) => {
  const { name, url, homeworld } = rawPilot;

  return {
    name,
    url,
    homeworld,
  };
};

export default { parsePilot };
