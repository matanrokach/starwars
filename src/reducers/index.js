import { combineReducers } from 'redux';
import tableReducer from './tableReducer';
import chartReducer from './chartReducer';

export default combineReducers({
  table: tableReducer,
  chart: chartReducer,
});
