import { ActionTypes } from '../constants';

const INIT_STATE = {
  vehicleWithMaxPopulation: {
    homePlanets: {},
  },
  isLoading: true,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ActionTypes.GET_VEHICLES_WITH_PILOTS_SUCCESS:
      return {
        ...state,
        vehicleWithMaxPopulation: action.payload.vehicleWithMaxPopulation,
        isLoading: false,
      };
    default:
      return state;
  }
};
