import { ActionTypes } from '../constants';

const INIT_STATE = {
  planets: [],
  maxPopulation: 0,
  isLoading: true,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case ActionTypes.GET_PLANETS_FOR_CHARTS_SUCCESS:
      return {
        ...state,
        planets: action.payload.planets,
        maxPopulation: action.payload.maxPopulation,
        isLoading: false,
      };
    default:
      return state;
  }
};
