const fetchData = (url) => {
  return fetch(url).then((response) => {
    return response.json();
  });
};

const baseUrl = 'https://swapi.dev/api';
const Urls = {
  Vehicles: `${baseUrl}/vehicles/`,
  People: `${baseUrl}/people/`,
  Planets: `${baseUrl}/planets/`,
};

const fetchAll = async (url) => {
  let actualUrl = url;
  let results = [];
  let data;

  data = await fetchData(actualUrl);
  results = [...results, ...data.results];
  actualUrl = data.next;

  if (!data.next) {
    return results;
  }

  return [...results, ...(await fetchAll(data.next))];
};

const fetchAllVehicles = () => {
  return fetchAll(Urls.Vehicles);
};

const fetchAllPeople = () => {
  return fetchAll(Urls.People);
};

const fetchAllPlanets = () => {
  return fetchAll(Urls.Planets);
};

const fetchByUrls = (urls) => {
  return Promise.all(urls.map((url) => fetchData(url)));
};

const fetchPlanetsByNames = (names) => {
  const urls = names.map((name) => `${Urls.Planets}?search=${name}`);
  return fetchByUrls(urls);
};

export default {
  fetchAllVehicles,
  fetchAllPeople,
  fetchAllPlanets,
  fetchByUrls,
  fetchPlanetsByNames,
};
