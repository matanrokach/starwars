import React from 'react';
import { ActivityIndicator, StyleSheet, View } from 'react-native';

const Loader = () => {
	return (
		<View style={Styles.container}>
			<ActivityIndicator
				size='large'
			/>
		</View>
	);
}

const Styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
	},
})

export default Loader;