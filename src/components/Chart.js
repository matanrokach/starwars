import React, { useEffect, useState } from 'react';
import {
  StyleSheet,
  View,
  Text,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import { Colors } from '../constants';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const Chart = ({ data, height: chartHeight, barOffset }) => {
  const [height, setHeight] = useState(0);

  const maxValue = Math.max(...data.map(({ value }) => value));

  useEffect(() => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
    setHeight(chartHeight);

    return () => setHeight(0);
  }, []);

  const chartColumn = ({ key, value }, index) => {
    const barHeight =
      barOffset + ((height - barOffset) * (value || 0)) / maxValue;

    const barColor = () => ({ backgroundColor: Colors[index % Colors.length] });

    return (
      <>
        <View style={[Styles.chartColumn, { height: barHeight }, barColor()]}>
          <Text>{key}</Text>
          <Text>{value}</Text>
        </View>
      </>
    );
  };

  return (
    <View style={[Styles.chart, { height: height }]}>
      {data.map(chartColumn)}
    </View>
  );
};

const Styles = StyleSheet.create({
  chart: {
    flexDirection: 'row',
    backgroundColor: 'white',
    alignItems: 'flex-end',
  },
  chartColumn: {
    flexDirection: 'column',
    backgroundColor: 'gray',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: 1,
  },
});

export default Chart;
