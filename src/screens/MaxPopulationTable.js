import React, { useState, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import {
  View,
  Text,
  StyleSheet,
  LayoutAnimation,
  Platform,
  UIManager,
} from 'react-native';
import { tableActions } from '../actions';
import { Loader } from '../components';

if (Platform.OS === 'android') {
  if (UIManager.setLayoutAnimationEnabledExperimental) {
    UIManager.setLayoutAnimationEnabledExperimental(true);
  }
}

const ROW_HEIGHT = 100;

const MaxPopulationTable = (props) => {
	const dispatch = useDispatch();
	
	const [height, setHeight] = useState(0);

  const vehicleWithMaxPopulation = useSelector(
    (state) => state.table.vehicleWithMaxPopulation,
  );
  const isLoading = useSelector((state) => state.table.isLoading);

  useEffect(() => {
    dispatch(tableActions.getVehiclesWithPilots());
		LayoutAnimation.configureNext(LayoutAnimation.Presets.spring);
		setHeight(ROW_HEIGHT);
		return () => setHeight(0);
  }, [isLoading]);

  const data = [
    {
      label: 'Vehicle name with the largest sum',
      content: vehicleWithMaxPopulation.name,
    },
    {
      label: 'Related home planets and their respective population',
      content: Object.values(vehicleWithMaxPopulation.homePlanets).map(
        (homePlanet) => `${homePlanet.name}: ${homePlanet.population}`,
      ),
    },
    {
      label: 'Related pilot names',
      content: vehicleWithMaxPopulation.pilotsNames,
    },
  ];

  const tableRow = (label, content) => {
    return (
      <View style={[Styles.tableRow, { height }]}>
        <View style={[Styles.tableCell, Styles.headerCell]}>
          <Text style={Styles.headerCellText}>{label}</Text>
        </View>
        <View style={Styles.tableCell}>
          <Text>{content}</Text>
        </View>
      </View>
    );
  };

  if (isLoading) {
    return <Loader />;
  }

  return (
    <View style={[Styles.container]}>
			{data.map(({ label, content }) => tableRow(label, content))}
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
    padding: 5,
  },
  tableRow: {
    flexDirection: 'row',
  },
  tableCell: {
    borderColor: 'gray',
    borderWidth: 1,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerCell: {
    backgroundColor: '#aadafa',
  },
  headerCellText: {
    fontWeight: 'bold',
  },
});

export default MaxPopulationTable;
