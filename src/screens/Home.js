import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from 'react-native';
import { Routes } from '../constants';

const navData = [
  {
    label: 'Table',
    color: '#afebce',
    route: Routes.MaxPopulationTable,
  },
  {
    label: 'Chart',
    color: '#faaaaa',
    route: Routes.PopulationChart,
  },
];

const Home = (props) => {
  const NavItem = ({ label, color: backgroundColor, route }) => {
    return (
      <TouchableOpacity
        style={[Styles.button, { backgroundColor }]}
        onPress={() => props.navigation.navigate(route)}>
        <Text>{label}</Text>
      </TouchableOpacity>
    );
  };

  return <View style={Styles.container}>{navData.map(NavItem)}</View>;
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
  },
  button: {
    height: 100,
    justifyContent: 'center',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: 'gray',
    margin: 4,
  },
});

export default Home;
