import Home from './Home';
import MaxPopulationTable from './MaxPopulationTable';
import PopulationChart from './PopulationChart';

export { Home, MaxPopulationTable, PopulationChart };
