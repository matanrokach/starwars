import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { View, StyleSheet } from 'react-native';
import { chartActions } from '../actions';
import { Loader, Chart } from '../components';

const planetNames = ['Tatooine', 'Alderaan', 'Naboo', 'Bespin', 'Endor'];

const CHART_HEIGHT = 600;
const BAR_OFFSET = 100;

const PopulationChart = (props) => {
  const dispatch = useDispatch();

  const planets = useSelector((state) => state.chart.planets);
  const isLoading = useSelector((state) => state.chart.isLoading);

  useEffect(() => {
    dispatch(chartActions.getPlanetsByNames(planetNames));
  }, []);

  if (isLoading) {
    return <Loader />;
  }

  const data = planets.map(({ name, population }) => ({
    key: name,
    value: population,
  }));

  return (
    <View style={Styles.container}>
      <Chart data={data} height={CHART_HEIGHT} barOffset={BAR_OFFSET} />
    </View>
  );
};

const Styles = StyleSheet.create({
  container: {
    flex: 1,
    width: '100%',
    backgroundColor: 'white',
    justifyContent: 'center',
    alignContent: 'center',
  },
});

export default PopulationChart;
