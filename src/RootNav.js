import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { Home, MaxPopulationTable, PopulationChart } from './screens';
import { Routes } from './constants';

const Stack = createStackNavigator();

const RootNav = (props) => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName={Routes.Home}>
        <Stack.Screen name={Routes.Home} component={Home} />
        <Stack.Screen
          name={Routes.MaxPopulationTable}
          component={MaxPopulationTable}
        />
        <Stack.Screen
          name={Routes.PopulationChart}
          component={PopulationChart}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNav;
